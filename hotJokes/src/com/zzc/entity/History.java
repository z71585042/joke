package com.zzc.entity;

import java.util.Date;

/**
 * 配置
 * @author zhengzhichao
 *
 */
public class History {
	private String jokeId;
	private int value;
	private Date updateDate;
	
	public String getJokeId() {
		return jokeId;
	}
	public void setJokeId(String jokeId) {
		this.jokeId = jokeId;
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
}
