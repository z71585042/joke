package com.zzc.entity;

import java.util.HashMap;
import java.util.Map;

public class Joke {
	private String jokeId;
	private String content;
	private int up;
	private int down;
	
	/**
	 * 转化为map
	 * @return
	 */
	public Map<String, Object> getJokeMap(){
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("jokeId", this.getJokeId());
		map.put("content", this.getContent());
		map.put("up", this.getUp());
		map.put("down", this.getDown());
		return map;
	}
	
	public String getJokeId() {
		return jokeId;
	}
	public void setJokeId(String jokeId) {
		this.jokeId = jokeId;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}

	public int getUp() {
		return up;
	}

	public void setUp(int up) {
		this.up = up;
	}

	public int getDown() {
		return down;
	}

	public void setDown(int down) {
		this.down = down;
	}
}
