package com.zzc.common;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 辅助类
 * @author zhengzhichao
 *
 */
public class DateUtil {
	private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	/**
	 * 将字符转化为日期
	 * @param date
	 * @return
	 */
	public static Date getDate(String date){
		Date ret = null;
		try {
			ret =  simpleDateFormat.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return ret;
	}
}
