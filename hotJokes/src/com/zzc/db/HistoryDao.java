package com.zzc.db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

/**
 * 提供常用方法
 * @author zhengzhichao
 *
 */
public class HistoryDao {
	private DBHelper helper;  
	private SQLiteDatabase db;  
	  
	public HistoryDao(Context context) {  
	    helper = new DBHelper(context);  
	    //因为getWritableDatabase内部调用了mContext.openOrCreateDatabase(mName, 0, mFactory);  
	    //所以要确保context已初始化,我们可以把实例化DBManager的步骤放在Activity的onCreate里  
	    db = helper.getWritableDatabase();  
	}
	
	/**
	 * 根据key获取value值
	 * 1.查询不到返回0
	 * 2.默认一个key只有一个value，如果出现多行记录，则只返回一个
	 * @param key
	 * @return
	 */
	public int getValue(String jokeId){
		int value = 0;
		String sql = "SELECT value FROM history WHERE joke_id = ?";
		String[] args = {jokeId};
		Cursor cursor = db.rawQuery(sql, args);
		while(cursor.moveToNext()){
			value = cursor.getInt(0);
		}
		return value;
	}
	
	/**
	 * 添加或者更新
	 * 次方法总会删除老的记录，增加新的记录
	 * @param jokeId
	 * @param value
	 */
	public void addAndUpdate(String jokeId,int value){
		db.beginTransaction();
		try {
			String delSql = "DELETE FROM history WHERE joke_id = ?";
			String addSql = "INSERT INTO history VALUES(?,?,datetime('now'))";
			db.execSQL(delSql, new Object[]{jokeId});
			db.execSQL(addSql, new Object[]{jokeId,value});
			
			db.setTransactionSuccessful();
		}finally{
			db.endTransaction();
		}
	}
	
	/**
	 * 删除一周之前的数据
	 */
	public void delOneWeekAgoData(){
		String sql = "DELETE FROM history WHERE joke_id IN(SELECT a.joke_id FROM history As a WHERE DATE(a.update_date) < DATE('now','-7 day'))";
		
		db.beginTransaction();
		try {
			db.execSQL(sql);
			
			db.setTransactionSuccessful();
		}finally{
			db.endTransaction();
		}
	}
	
}
