package com.zzc.db;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.zzc.entity.Joke;

/**
 * 提供常用方法
 * @author zhengzhichao
 *
 */
public class JokeDao {
	private DBHelper helper;  
	private SQLiteDatabase db;  
	  
	public JokeDao(Context context) {  
	    helper = new DBHelper(context);  
	    //因为getWritableDatabase内部调用了mContext.openOrCreateDatabase(mName, 0, mFactory);  
	    //所以要确保context已初始化,我们可以把实例化DBManager的步骤放在Activity的onCreate里  
	    db = helper.getWritableDatabase();  
	}
	
	/**
	 * 添加
	 * @param jokes
	 */
	public void add(List<Joke> jokes){
		/*StringBuilder insertSql = new StringBuilder("INSERT INTO joke VALUES");
		Object[] args = new Object[jokes.size()*2];
		int i = 0;
		for(Joke temp : jokes){
			insertSql.append("(?,?),");
			args[i++] = temp.getJokeId();
			args[i++] = temp.getContent();
		}
		
		String sql = insertSql.substring(0, insertSql.length()-1);
		
		db.execSQL(sql, args);*/
		
		db.beginTransaction();
		try {  
            for (Joke temp : jokes) {  
                db.execSQL("INSERT INTO joke VALUES(?,?,?,?)", new Object[]{temp.getJokeId(),temp.getContent(),temp.getUp(),temp.getDown()});  
            }  
            db.setTransactionSuccessful();  //设置事务成功完成  
        } finally {  
            db.endTransaction();    //结束事务  
        }  
	}
	
	/**
	 * 删除所有
	 */
	public void delAll(){
		String sql = "DELETE FROM joke";
		db.execSQL(sql);
	}
	
	/**
	 * 查询所有
	 * @return
	 */
	public List<Joke> queryAllDesc(){
		List<Joke> jokes = new ArrayList<Joke>();
		String sql = "SELECT joke_id,content,up,down FROM joke ORDER BY joke_id DESC";
		Cursor cursor = db.rawQuery(sql, null);
		while(cursor.moveToNext()){
			Joke joke = new Joke();
			joke.setJokeId(cursor.getString(0));
			joke.setContent(cursor.getString(1));
			joke.setUp(cursor.getInt(2));
			joke.setDown(cursor.getInt(3));
			jokes.add(joke);
		}
		return jokes;
	}
}
