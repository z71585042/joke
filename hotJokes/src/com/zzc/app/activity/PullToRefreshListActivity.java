/*******************************************************************************
 * Copyright 2011, 2012 Chris Banes.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.zzc.app.activity;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ListActivity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.handmark.pulltorefresh.library.PullToRefreshBase.State;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.handmark.pulltorefresh.library.extras.SoundPullEventListener;
import com.zzc.db.HistoryDao;
import com.zzc.db.JokeDao;
import com.zzc.entity.Joke;

public final class PullToRefreshListActivity extends ListActivity {
	
	//访问地址
	private final String BASE_URL = "http://www.zhengzhichao.com.cn";
//	private final String BASE_URL = "http://192.168.163.48:8080/tool";
	
	private final String URL_MORE_JOKES = BASE_URL+"/app/getMoreJokes";
	private final String URL_NEWEST_JOKES = BASE_URL+"/app/getNewestJokes";
	private final String URL_UP = BASE_URL+"/app/up";
	private final String URL_DOWN = BASE_URL+"/app/down";
	
	//能够显示的joke最大数量
	private final int MAX_JOKE_LENGTH = 99999;
	
//	//最后一个jokeId 用来实现快速查询
	private String lastJokeId = "0";
	//最新的jokeId 可以认为是最上面
	private String firstJokeId = "0";
	//是否是获取最新的信息
	private Boolean isGetNewest = false;
	
	private JokeDao jokeDao = null;
	private HistoryDao historyDao = null;

	static final int MENU_MANUAL_REFRESH = 0;
	static final int MENU_DISABLE_SCROLL = 1;
	static final int MENU_SET_MODE = 2;
	static final int MENU_DEMO = 3;

	private LinkedList<Map<String,Object>> mListItems;
	private PullToRefreshListView mPullRefreshListView;
//	private ArrayAdapter<Joke> mAdapter;
	private SimpleAdapter mAdapter;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ptr_list);//设置布局

		mPullRefreshListView = (PullToRefreshListView) findViewById(R.id.pull_refresh_list);
		//设置双向滑动
		mPullRefreshListView.setMode(Mode.BOTH);
		
		//初始化
		this.jokeDao = new JokeDao(this.getApplicationContext());
		this.historyDao = new HistoryDao(this.getApplicationContext());
		
		// Set a listener to be invoked when the list should be refreshed.
		/**
		 * 添加滑动刷新时候的监听事件
		 */
		mPullRefreshListView.setOnRefreshListener(new OnRefreshListener2<ListView>() {

			/**
			 * 下拉的时候加载最新段子
			 * @param refreshView
			 */
			@Override
			public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
				String label = DateUtils.formatDateTime(getApplicationContext(), System.currentTimeMillis(),
						DateUtils.FORMAT_SHOW_TIME | DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_ABBREV_ALL);

				// Update the LastUpdatedLabel
				refreshView.getLoadingLayoutProxy().setLastUpdatedLabel(label);

				isGetNewest = true;
				
				// Do work to refresh the list here.
				
				new GetDataTask().execute("0");
			}

			/**
			 * 上拉的时候加载更多
			 * @param refreshView
			 */
			@Override
			public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
				String label = DateUtils.formatDateTime(getApplicationContext(), System.currentTimeMillis(),
						DateUtils.FORMAT_SHOW_TIME | DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_ABBREV_ALL);

				// Update the LastUpdatedLabel
				refreshView.getLoadingLayoutProxy().setLastUpdatedLabel(label);

				//判断是否超过最大数量
				if(mListItems.size() > MAX_JOKE_LENGTH){
					Toast.makeText(PullToRefreshListActivity.this, "亲，已到达结尾!", Toast.LENGTH_SHORT).show();
				}else{
					isGetNewest = false;
					
					// Do work to refresh the list here.
					new GetDataTask().execute("1");
				}
				
			}
			/*@Override
			public void onRefresh(PullToRefreshBase<ListView> refreshView) {
				String label = DateUtils.formatDateTime(getApplicationContext(), System.currentTimeMillis(),
						DateUtils.FORMAT_SHOW_TIME | DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_ABBREV_ALL);

				// Update the LastUpdatedLabel
				refreshView.getLoadingLayoutProxy().setLastUpdatedLabel(label);

				// Do work to refresh the list here.
				new GetDataTask().execute();
			}*/
		});

		// Add an end-of-list listener
		/**
		 * 添加滑动到最后的监听事件
		 */
		/*mPullRefreshListView.setOnLastItemVisibleListener(new OnLastItemVisibleListener() {

			@Override
			public void onLastItemVisible() {
				Toast.makeText(PullToRefreshListActivity.this, "End of List!", Toast.LENGTH_SHORT).show();
			}
		});*/

		ListView actualListView = mPullRefreshListView.getRefreshableView();

		// Need to use the Actual ListView when registering for Context Menu
		registerForContextMenu(actualListView);

		mListItems = new LinkedList<Map<String,Object>>();
//		mListItems.addAll(Arrays.asList(mStrings));

//		mAdapter = new ArrayAdapter<Joke>(this, R.layout.simpletest, mListItems);
		mAdapter = new SimpleAdapter(this, this.mListItems, R.layout.simpletest, new String[]{"content"}, new int[]{R.id.content}){
			/**
			 * 重写此方法，为按钮注册事件
			 */
			@Override
			public View getView(final int position, View convertView, ViewGroup parent) {
				//调用父类方法
				convertView =  super.getView(position, convertView, parent);
				
				final Map<String,Object> dataItem = (Map<String,Object>)getItem(position);//获取当前绑定的数据模型
				
				/**
				 * 获取up down按钮,注册点击事件
				 */
				final Button upButton = (Button)convertView.findViewById(R.id.up);
				upButton.setText(String.valueOf((Integer)dataItem.get("up")));
				
				final Button downButton = (Button)convertView.findViewById(R.id.down);
				downButton.setText(String.valueOf((Integer)dataItem.get("down")));
				
				final String itemJokeId = (String)dataItem.get("jokeId");//当前item的jokeId
				//获取点击历史
				int clickValue = historyDao.getValue(itemJokeId);
				if(clickValue == 1){//点击过up
					upButton.setEnabled(false);//不可用
					downButton.setEnabled(true);
				}else if(clickValue == -1){//点击过down
					upButton.setEnabled(true);
					downButton.setEnabled(false);//不可用
				}else{
					upButton.setEnabled(true);//可用
					downButton.setEnabled(true);
				}
				
				//up按钮注册点击事件
				upButton.setOnClickListener(new View.OnClickListener(){

					@Override
					public void onClick(View v) {
//						Map<String,Object> dataItem = (Map<String,Object>)getItem(position);//获取当前绑定的数据模型
						
						Log.d(this.getClass().getName(),  new JSONObject(dataItem).toString());
						
						/**
						 * 获取up down按钮,注册点击事件
						 */
						
//						Button upButton = (Button)v.findViewById(R.id.up);
//						Button downButton = (Button)v.findViewById(R.id.down);
						
//						String itemJokeId = (String)dataItem.get("jokeId");//当前item的jokeId
						//获取点击历史
						int clickValue = historyDao.getValue(itemJokeId);
						
						if(clickValue == -1){//点击过down
							//down设置为可用
							downButton.setEnabled(true);
							//撤销down记录
							sendDownRequest(itemJokeId,-1);
							//更新down按钮数字
							int downNum = Integer.valueOf(downButton.getText().toString());
							downNum++;
							downButton.setText(String.valueOf(downNum));
							//更新绑定数据
							dataItem.put("down", downNum);
						}else if(clickValue == 1){//点击过up
							//点击过up 理论上不存在这种情况，什么都不做
						}else{//都没点过
							//up down 按钮都是可用的
//							historyDao.addAndUpdate(itemJokeId, 1);//设置为点击过
							
						}
						
						//设置为点击过up按钮
						historyDao.addAndUpdate(itemJokeId, 1);
						//up按钮不能再次点击
						upButton.setEnabled(false);
						//发送up请求
						sendUpRequest(itemJokeId, 1);
						//更新up按钮数字
						int upNum = Integer.valueOf(upButton.getText().toString());
						upNum++;
						upButton.setText(String.valueOf(upNum));
						//更新绑定数据
						dataItem.put("up",upNum);
					}
					
				});
				
				//down按钮注册点击事件
				downButton.setOnClickListener(new View.OnClickListener(){
					@Override
					public void onClick(View v) {
//						Map<String,Object> dataItem = (Map<String,Object>)getItem(position);//获取当前绑定的数据模型
						
						Log.d(this.getClass().getName(),  new JSONObject(dataItem).toString());
						
						/**
						 * 获取up down按钮,注册点击事件
						 */
//						Button upButton = (Button)v.findViewById(R.id.up);
//						Button downButton = (Button)v.findViewById(R.id.down);
						
//						String itemJokeId = (String)dataItem.get("jokeId");//当前item的jokeId
						//获取点击历史
						int clickValue = historyDao.getValue(itemJokeId);
						
						if(clickValue == -1){//点击过down 论上不存在这种情况，什么都不做
							
						}else if(clickValue == 1){//点击过up
							//up设置为可用
							upButton.setEnabled(true);
							//撤销up记录
							sendUpRequest(itemJokeId,-1);
							//更新up按钮数字
							int upNum = Integer.valueOf(upButton.getText().toString());
							upNum--;
							upButton.setText(String.valueOf(upNum));
							//更新绑定数据
							dataItem.put("up",upNum);
						}else{//都没点过
							//up down 按钮都是可用的
//							historyDao.addAndUpdate(itemJokeId, 1);//设置为点击过
							
						}
						
						//设置为点击过down按钮
						historyDao.addAndUpdate(itemJokeId, -1);
						//down按钮不能再次点击
						downButton.setEnabled(false);
						//发送down请求
						sendDownRequest(itemJokeId, 1);
						//更新down按钮数字
						int downNum = Integer.valueOf(downButton.getText().toString());
						downNum--;
						downButton.setText(String.valueOf(downNum));
						//更新绑定数据
						dataItem.put("down", downNum);
					}
					
				});
				
				/**
				 * 分享按钮
				 */
				
				final Button shareButton = (Button)convertView.findViewById(R.id.share);
				shareButton.setOnClickListener(new View.OnClickListener(){

					@Override
					public void onClick(View v) {
						Intent intent=new Intent(Intent.ACTION_SEND); 
						intent.setType("text/plain");
//						intent.setPackage("com.sina.weibo"); 
						intent.putExtra(Intent.EXTRA_SUBJECT, "主题-subject"); 
						intent.putExtra(Intent.EXTRA_TEXT, dataItem.get("content").toString());
						intent.putExtra(Intent.EXTRA_TITLE, "标题-title");
						intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK); 
						startActivity(Intent.createChooser(intent, "请选择")); 
					}
					
				});
				
				
				return convertView;
			}
		};

		/**
		 * Add Sound Event Listener
		 */
		SoundPullEventListener<ListView> soundListener = new SoundPullEventListener<ListView>(this);
		soundListener.addSoundEvent(State.PULL_TO_REFRESH, R.raw.pull_event);
		soundListener.addSoundEvent(State.RESET, R.raw.reset_sound);
		soundListener.addSoundEvent(State.REFRESHING, R.raw.refreshing_sound);
		mPullRefreshListView.setOnPullEventListener(soundListener);

		// You can also just use setListAdapter(mAdapter) or
		// mPullRefreshListView.setAdapter(mAdapter)
		actualListView.setAdapter(mAdapter);
		
		/**
		 * 注册up down 按钮事件s
		 */
		
		/**
		 * 加载本地缓存段子
		 */
		List<Joke> jokes = this.jokeDao.queryAllDesc();
		if(jokes != null && jokes.size() > 0){
			mListItems.addAll(getData(jokes));
			//初始化
			this.firstJokeId = jokes.get(0).getJokeId();
			this.lastJokeId = jokes.get(jokes.size()-1).getJokeId();
		}
		
	}
	
	/**
	 * 发送up请求
	 * @param jokeId
	 * @param count
	 */
	private void sendUpRequest(final String jokeId,final int count){
		/**
		 * 发送请求修改up次数
		 * 此处直接发送请求，不管是否调用成功
		 */
		
		new Thread(){
			@Override
			public void run() {
				HttpClient httpClient = new DefaultHttpClient();
	        	HttpPost httpPost = new HttpPost(URL_UP);
	        	
	        	List<BasicNameValuePair> param = new ArrayList<BasicNameValuePair>();
	        	param.add(new BasicNameValuePair("jokeId", jokeId));
	        	param.add(new BasicNameValuePair("count", String.valueOf(count)));
	        	Log.d(this.getClass().getName(), "lastJokeId = "+lastJokeId);
	        	Log.d(this.getClass().getName(), "jokeId = "+jokeId);
	        	Log.d(this.getClass().getName(), "count = "+count);
	        	try {
					httpPost.setEntity(new UrlEncodedFormEntity(param,"utf-8"));
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
				try {
					httpClient.execute(httpPost);
				} catch (ClientProtocolException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
		}.start();
	}
	
	/**
	 * 发送down请求
	 * @param jokeId
	 * @param count
	 */
	private void sendDownRequest(final String jokeId,final int count){
		/**
		 * 发送请求修改down次数
		 * 此处直接发送请求，不管是否调用成功
		 */
		new Thread(){
			@Override
			public void run() {
				HttpClient httpClient = new DefaultHttpClient();
	        	HttpPost httpPost = new HttpPost(URL_DOWN);
	        	
	        	List<BasicNameValuePair> param = new ArrayList<BasicNameValuePair>();
	        	param.add(new BasicNameValuePair("jokeId", jokeId));
	        	param.add(new BasicNameValuePair("count", String.valueOf(count)));
	        	Log.d(this.getClass().getName(), "lastJokeId = "+lastJokeId);
	        	Log.d(this.getClass().getName(), "jokeId = "+jokeId);
	        	Log.d(this.getClass().getName(), "count = "+count);
	        	try {
					httpPost.setEntity(new UrlEncodedFormEntity(param,"utf-8"));
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
				try {
					httpClient.execute(httpPost);
				} catch (ClientProtocolException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (ParseException e) {
					e.printStackTrace();
				}
				
				//更新本地
			}
		}.start();
	}
	
	/**
	 * 获取绑定数据
	 * @return
	 */
	private List<Map<String, Object>> getData(List<Joke> jokes){
		List<Map<String, Object>> ret = new ArrayList<Map<String,Object>>();
		for(Joke joke : jokes){
			ret.add(joke.getJokeMap());
		}
		return ret;
	}
	
	/**
	 * 异步任务
	 * 异步加载端子内容
	 * @author zhengzhichao
	 *
	 */
	private class GetDataTask extends AsyncTask<String, Void, List<Joke>> {

		@Override
        protected List<Joke> doInBackground(String... params) {
			List<Joke> rets = new ArrayList<Joke>();
            // Simulates a background job.
			/**
        	 * 发送http请求
        	 */
			if("0".equals(params[0])){//获取最新
				HttpClient httpClient = new DefaultHttpClient();
	        	HttpPost httpPost = new HttpPost(URL_NEWEST_JOKES);
	        	
	        	try {
					HttpResponse response = httpClient.execute(httpPost);
					if(response.getStatusLine().getStatusCode() == 200){//请求成功
						JSONObject jsonObject = new JSONObject(EntityUtils.toString(response.getEntity(),"utf-8"));
						if("SUCCESS".equals(jsonObject.getString("status"))){//返回成功
							JSONArray jsonDatas = jsonObject.getJSONArray("data");
							
							if(jsonDatas.length() > 0){
								String jokeId = jsonDatas.getJSONObject(0).getString("jokeId");
								if(firstJokeId.equals(jokeId)){//已经是最新
									
								}else{//非最新
									for(int i = 0 ;i < jsonDatas.length() ; i++){
										Joke temp = new Joke();
										temp.setJokeId(jsonDatas.getJSONObject(i).getString("jokeId"));
										temp.setContent(jsonDatas.getJSONObject(i).getString("content"));
										temp.setUp(jsonDatas.getJSONObject(i).getInt("upCount"));
										temp.setDown(jsonDatas.getJSONObject(i).getInt("downCount"));
										rets.add(temp);
									}
									
									firstJokeId = jokeId;
									lastJokeId = jsonDatas.getJSONObject(jsonDatas.length()-1).getString("jokeId");
								}
							}
						}
					}
				} catch (ClientProtocolException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (ParseException e) {
					e.printStackTrace();
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}else{//加载更多
				HttpClient httpClient = new DefaultHttpClient();
	        	HttpPost httpPost = new HttpPost(URL_MORE_JOKES);
	        	
	        	List<BasicNameValuePair> param = new ArrayList<BasicNameValuePair>();
	        	param.add(new BasicNameValuePair("s", lastJokeId));
	        	Log.d(this.getClass().getName(), "lastJokeId = "+lastJokeId);
	        	try {
					httpPost.setEntity(new UrlEncodedFormEntity(param,"utf-8"));
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
	        	
	        	try {
					HttpResponse response = httpClient.execute(httpPost);
					if(response.getStatusLine().getStatusCode() == 200){//请求执行成功
						JSONObject jsonObject = new JSONObject(EntityUtils.toString(response.getEntity(),"utf-8"));
						if("SUCCESS".equals(jsonObject.getString("status"))){
							JSONArray jsonDatas = jsonObject.getJSONArray("data");
							for(int i = 0 ;i < jsonDatas.length() ; i++){
								Joke temp = new Joke();
								temp.setJokeId(jsonDatas.getJSONObject(i).getString("jokeId"));
								temp.setContent(jsonDatas.getJSONObject(i).getString("content"));
								temp.setUp(jsonDatas.getJSONObject(i).getInt("upCount"));
								temp.setDown(jsonDatas.getJSONObject(i).getInt("downCount"));
								rets.add(temp);
							}
							
							if(jsonDatas.length() > 0){
								lastJokeId = jsonDatas.getJSONObject(jsonDatas.length()-1).getString("jokeId");
							}
						}
					}
				} catch (ClientProtocolException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (ParseException e) {
					e.printStackTrace();
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
			
			return rets;
		 }

		@Override
		protected void onPostExecute(List<Joke> result) {
			/**
        	 * 刷新
        	 */
			if(isGetNewest){//如果是要请求最新
				if(result.size() > 0){
					//更新显示内容
					mListItems.clear();
					for(int i = result.size()-1;i >= 0 ;i--){
						mListItems.addFirst(result.get(i).getJokeMap());
					}
				}else{
					Toast.makeText(PullToRefreshListActivity.this, "已经为最新!", Toast.LENGTH_SHORT).show();
				}
			}else{//加载更多
				for(Joke temp : result){
					mListItems.addLast(temp.getJokeMap());
				}
			}
        	
			mAdapter.notifyDataSetChanged();//通知更新

			// Call onRefreshComplete when the list has been refreshed.
			mPullRefreshListView.onRefreshComplete();

			super.onPostExecute(result);
		}
	}
	
	@Override
	public void onPause() {
		Log.d(this.getClass().getName(), "----->pause!");
		//结束前将部分端子缓存入数据库
		if(mListItems.size() > 0){
//			int size = this.mListItems.size() >= 10 ? 10 : this.mListItems.size();
			List<Joke> jokes = new ArrayList<Joke>(); 
			for(Map<String, Object> temp :this.mListItems){
				Joke joke = new Joke();
				joke.setJokeId((String)temp.get("jokeId"));
				joke.setContent((String)temp.get("content"));
				joke.setUp(Integer.valueOf(temp.get("up").toString()));
				joke.setDown(Integer.valueOf(temp.get("down").toString()));
				jokes.add(joke);
			}
			this.jokeDao.delAll();
			this.jokeDao.add(jokes);
		}
		
		super.onPause();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0, MENU_MANUAL_REFRESH, 0, "Manual Refresh");
		menu.add(0, MENU_DISABLE_SCROLL, 1,
				mPullRefreshListView.isScrollingWhileRefreshingEnabled() ? "Disable Scrolling while Refreshing"
						: "Enable Scrolling while Refreshing");
		menu.add(0, MENU_SET_MODE, 0, mPullRefreshListView.getMode() == Mode.BOTH ? "Change to MODE_PULL_DOWN"
				: "Change to MODE_PULL_BOTH");
		menu.add(0, MENU_DEMO, 0, "Demo");
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) menuInfo;

		menu.setHeaderTitle("Item: " + getListView().getItemAtPosition(info.position));
		menu.add("Item 1");
		menu.add("Item 2");
		menu.add("Item 3");
		menu.add("Item 4");

		super.onCreateContextMenu(menu, v, menuInfo);
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		MenuItem disableItem = menu.findItem(MENU_DISABLE_SCROLL);
		disableItem
				.setTitle(mPullRefreshListView.isScrollingWhileRefreshingEnabled() ? "Disable Scrolling while Refreshing"
						: "Enable Scrolling while Refreshing");

		MenuItem setModeItem = menu.findItem(MENU_SET_MODE);
		setModeItem.setTitle(mPullRefreshListView.getMode() == Mode.BOTH ? "Change to MODE_FROM_START"
				: "Change to MODE_PULL_BOTH");

		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
			case MENU_MANUAL_REFRESH:
				new GetDataTask().execute();
				mPullRefreshListView.setRefreshing(false);
				break;
			case MENU_DISABLE_SCROLL:
				mPullRefreshListView.setScrollingWhileRefreshingEnabled(!mPullRefreshListView
						.isScrollingWhileRefreshingEnabled());
				break;
			case MENU_SET_MODE:
				mPullRefreshListView.setMode(mPullRefreshListView.getMode() == Mode.BOTH ? Mode.PULL_FROM_START
						: Mode.BOTH);
				break;
			case MENU_DEMO:
				mPullRefreshListView.demo();
				break;
		}

		return super.onOptionsItemSelected(item);
	}

	private String[] mStrings = { "请滑动加载最新段子" };
}
